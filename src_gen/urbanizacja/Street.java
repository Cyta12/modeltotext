package urbanizacja;

public class Street   implements NumbersHaver
 {
	// Atributes:
	private int	Numbers;
	private String	Name;

	// Getters & Setters:
	public int getNumbers() { 
		return Numbers; }
	public void setNumbers(int w) { 
		Numbers = w; }
	public String getName() { 
		return Name; }
	public void setName(String w) { 
		Name = w; }

	// Methods:	
	public boolean isNumber() {
			return !Numbers.empty();
		
	}
	public void crossing()
 {
			Name = "skrzyżowanie";
		
	}
	
};
